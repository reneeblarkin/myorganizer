package edu.depaul.csc472.myorganizer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class ToDoList extends Activity {

    private static final String TAG = "ToDoList";
    Button buttonNewTask;
    ListView list_item;
    EditText newTask;
    ArrayList<String> tasks;
    ArrayAdapter<String> taskAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);

        Log.d(TAG, "onCreate");


        buttonNewTask = (Button) findViewById(R.id.buttonNewTask);
        list_item = (ListView) findViewById(R.id.list_item);
        tasks = new ArrayList<String>();
        taskAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        readTasksFromFile();
        list_item.setAdapter(taskAdapter);


        //adding item to list
        tasks.add("First Task");

        //remove task listsner
        removeTaskListener();

    }

    private void removeTaskListener(){
        Log.d(TAG, "removeTaskListener");
        list_item.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapter, View task, int postion, long id) {
                        //removes task
                        taskAdapter.remove(tasks.get(postion));
                        tasks.remove(postion);
                        //updates adapter
                        taskAdapter.notifyDataSetChanged();
                        Log.d(TAG, "removeTaskListener called writeTasksToFile");
                        writeTasksToFile();
                        return true;
                    }
                });
    }

    public void addNewTask(View view){
        Log.d(TAG, "addNewTask");
        newTask = (EditText) findViewById(R.id.newTask);
        String taskText = newTask.getText().toString();
        tasks.add(taskText);
        taskAdapter.add(taskText);
        newTask.setText("");
        Log.d(TAG, "addNewTask called write to file");
        writeTasksToFile();

    }

    private void readTasksFromFile(){
        String fileName = "todolist";
        Log.d(TAG, getFilesDir().toString());

        try{
            FileInputStream fileInputStream = openFileInput(fileName);
            DataInputStream inputFromFile = new DataInputStream(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputFromFile));
            String temp;
            while((temp = bufferedReader.readLine()) != null){
                Log.d(TAG, "reading from file: "+ temp);
                tasks.add(temp);
                taskAdapter.add(temp);
            }
            Log.d(TAG, "readTasksFromFile read from file");

            fileInputStream.close();
        }
        catch(IOException ex){
            tasks = new ArrayList<String>();
            Log.d(TAG, "readTasksFromFile created new tasks list");
        }

    }

    private void writeTasksToFile(){
        String fileName = "todolist";
        try{
            FileOutputStream fileOutputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

            for(int i = 0; i < tasks.size(); i++){
                Log.d(TAG, "writing into file: "+tasks.get(i));
                bufferedWriter.write(tasks.get(i));
                bufferedWriter.newLine();
            }
            Log.d(TAG, "writeTasksToFile wrote tasks in file");
            bufferedWriter.close();
            fileOutputStream.close();


        }
        catch (IOException ex){
            Log.d(TAG, "writeTasksToFile did not write the tasks");
            ex.printStackTrace();
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_to_do_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
