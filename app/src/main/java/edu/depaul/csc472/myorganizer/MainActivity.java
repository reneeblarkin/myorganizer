package edu.depaul.csc472.myorganizer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    Button todoList;
    Button newEvent;
    CalendarView calendarView;
    ToDoListener toDoListener;
    NewEventListener newEventListener;
    TodayActivityListener todayActivityListener;
    String dateSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        todoList = (Button) findViewById(R.id.toDoList);
        newEvent = (Button) findViewById(R.id.newEvent);
        calendarView = (CalendarView) findViewById(R.id.calendarView);
        toDoListener = new ToDoListener();
        newEventListener = new NewEventListener();
        todayActivityListener = new TodayActivityListener();

        int[] buttonRequests = {R.id.calendarView, R.id.toDoList, R.id.newEvent};


        for(int request: buttonRequests ){
            View view = findViewById(request);
            action(view);
        }


    }
    private void action (View view){
        switch (view.getId()){
            case R.id.toDoList:
                view.setOnClickListener(toDoListener);
                break;
            case R.id.newEvent:
                view.setOnClickListener(newEventListener);
            default:
                calendarView.setOnDateChangeListener(todayActivityListener);

                break;
        }
    }

    private class ToDoListener implements View.OnClickListener{
        public void onClick(View view){
            //creating a toast that tells me that the button was clicked
            Context context = getApplicationContext();
            CharSequence text = "To Do Button was Clicked";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

            //starts the to do list activity
            Intent intent = new Intent(MainActivity.this, ToDoList.class);
            startActivity(intent);

        }
    }

    private class NewEventListener implements View.OnClickListener{
        public void onClick(View view){
            //creates a toast telling me that the new event button was clicked
            Context context = getApplicationContext();
            CharSequence text = "new event button was clicked";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            if(dateSelected == null){
                CharSequence text2 = "you must select a date";
                Toast toast1 = Toast.makeText(context, text2, duration);
                toast1.show();
            }
            else{
                Intent intent = new Intent(MainActivity.this, EventActivity.class);
                intent.putExtra("Date", dateSelected);
                startActivity(intent);
            }
        }
    }

    private class TodayActivityListener implements CalendarView.OnDateChangeListener{

        public void onSelectedDayChange(CalendarView view, int year, int month, int day){
            Context context = getApplicationContext();
            month = month + 1;
            CharSequence text = "new date selected! " + month + "/"+ day+ "/"+year;
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            dateSelected = month + "/" + day + "/" + year;
            Intent intent = new Intent(MainActivity.this, EventActivity.class);
            intent.putExtra("Date", dateSelected);
            startActivity(intent);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
