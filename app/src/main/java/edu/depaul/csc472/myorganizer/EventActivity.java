package edu.depaul.csc472.myorganizer;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;


public class EventActivity extends AppCompatActivity {
    private static final String TAG = "EventActivity";
    Button addNewEvent;
    ListView listEvents;
    EditText newEvent;
    TextView date;
    String dateString;
    ArrayList<String> events;
    ArrayAdapter<String> eventAdapter;
    TimeForReminder reminder;

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        Intent intent = getIntent();
        CharSequence tempDate;
        if (intent != null) {
            date = (TextView) findViewById(R.id.date);
            tempDate = intent.getCharSequenceExtra("Date");
            date.setText("Selected Date: " + tempDate);
            dateString = tempDate.toString().replace("/", ".");
        } else {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            dateString = month + "." + day + "." + year;
        }

        readEventsFromFile();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Log.d(TAG, "onCreate");

        addNewEvent = (Button) findViewById(R.id.buttonNewEvent);
        listEvents = (ListView) findViewById(R.id.listEvent);
        events = new ArrayList<String>();
        eventAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        listEvents.setAdapter(eventAdapter);

        events.add("first event");

        removeEventListener();
        updateToDoList();

    }

    public void addNewEvent(View view) {
        Log.d(TAG, "addNewEvent");
        newEvent = (EditText) findViewById(R.id.newEvent);
        String event = newEvent.getText().toString();
        events.add(event);
        eventAdapter.add(event);
        showTimePickerDialog(view);
        newEvent.setText("");
        Log.d(TAG, "addNewEvent writeEventsToFile was called");
        writeEventsToFile();
        updateToDoList();

    }

    private void removeEventListener() {
        Log.d(TAG, "removeTaskListener");
        listEvents.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapter, View task, int postion, long id) {
                        //removes task
                        eventAdapter.remove(events.get(postion));
                        events.remove(postion);
                        //updates adapter
                        eventAdapter.notifyDataSetChanged();
                        Log.d(TAG, "removeTaskListener writeEventsToFile Called");
                        writeEventsToFile();
                        return true;
                    }
                });
    }


    private void readEventsFromFile() {
        String fileName = "eventList" + dateString;

        try {
            FileInputStream fileInputStream = openFileInput(fileName);
            DataInputStream inputFromFile = new DataInputStream(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputFromFile));
            String temp;
            Log.d(TAG, "readEvents from file the file name is " + fileName);
            while ((temp = bufferedReader.readLine()) != null) {
                Log.d(TAG, "reading from file: " + temp);
                events.add(temp);
                eventAdapter.add(temp);
            }
            Log.d(TAG, "readEventsFromFile read from a file");
            fileInputStream.close();
        } catch (IOException ex) {
            Log.d(TAG, "readEventsFromFile did not read from a file" + fileName);
            events = new ArrayList<String>();
        }

    }

    private void writeEventsToFile() {
        String fileName = "eventList" + dateString;
        try {
            FileOutputStream fileOutputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
            for (int i = 0; i < events.size(); i++) {
                Log.d(TAG, "writing into file: " + events.get(i));
                bufferedWriter.write(events.get(i));
                bufferedWriter.newLine();

            }

            Log.d(TAG, "writeEventsToFile wrote to file");
            bufferedWriter.close();
            fileOutputStream.close();

        } catch (IOException ex) {
            Log.d(TAG, "writeEventsToFile did not write to the file");
            ex.printStackTrace();
        }

    }

    private void updateToDoList() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String todaysDate = month + "." + day + "." + year;
        Log.d(TAG, "updateToDoList todays date is: " + todaysDate + " dateString is:" + dateString);
        String fileName = "todolist";
        Log.d(TAG, getFilesDir().toString());

        if (todaysDate.equals(dateString)) {
            ArrayList<String> tempToDoList = new ArrayList<String>();
            try {
                //reading from the file
                FileInputStream fileInputStream = openFileInput(fileName);
                DataInputStream inputFromFile = new DataInputStream(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputFromFile));
                String temp;
                Log.d(TAG, "read tasks from file the file name is " + fileName);
                while ((temp = bufferedReader.readLine()) != null) {
                    Log.d(TAG, "reading from file: " + temp);
                    tempToDoList.add(temp);
                }
                Log.d(TAG, "readEventsFromFile read from a file");
                fileInputStream.close();
            } catch (IOException ex) {
                Log.d(TAG, "readEventsFromFile did not read from a file" + fileName);
                events = new ArrayList<String>();
            }
            //adding new event to list
            for (int i = 0; i < events.size(); i++) {
                tempToDoList.add(events.get(i));
            }
            try {
                //writing into the file
                FileOutputStream fileOutputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

                for (int i = 0; i < tempToDoList.size(); i++) {
                    Log.d(TAG, "writing into file: " + tempToDoList.get(i));
                    bufferedWriter.write(tempToDoList.get(i));
                    bufferedWriter.newLine();
                }
                Log.d(TAG, "writeTasksToFile wrote tasks in file");
                bufferedWriter.close();
                fileOutputStream.close();


            } catch (IOException ex) {
                Log.d(TAG, "updateToDoList todolist has not been updated.");
                ex.printStackTrace();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class TimeForReminder extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            //setting current time as default value
            final Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);

            return new TimePickerDialog(getActivity(), this, hour, minute, true);
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            //set reminder for the user. with the time choose and the dateString
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            CharSequence setReminder = "A reminder was set for "+hourOfDay+":"+minute+" on"+dateString;
            Toast toast = Toast.makeText(context, setReminder, duration);
            toast.show();

            /*

            My attempt to add push notifications.

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                    .setContentTitle("reminder for event"+ events.get(events.size() -1))
                    .setContentText(setReminder);
           Intent sendReminder = new Intent(context, EventActivity.class);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(EventActivity.class);
            PendingIntent reminderPendingIntent =
                    stackBuilder.getPendingIntent(
                            0, PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(reminderPendingIntent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            */




        }
    }

    public void showTimePickerDialog(View view){
        DialogFragment newFragment = new TimeForReminder();
        newFragment.show(getFragmentManager(), "timePicker");
    }
}
